<!DOCTYPE html>
<html lang="en-US" ng-app="cubeSum">
<head>
	<title>Prueba Maykol Purica</title>

	<!-- Load Bootstrap CSS -->
	<link href="<?= asset( 'app/css/bootstrap.min.css' ) ?>" rel="stylesheet">
</head>
<body>
<h2>Prueba Maykol Purica</h2>

<div ng-controller="cubeController">

	<!-- Table-to-load-the-data Part -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">

				<form>
					<div class="row">
						<div class="form-group col-xs-6">
							<label for="exampleInputEmail1">T</label>
							<input type="number" min="1" max="50" ng-model="testCases"
							       ng-init="testCases = 1"
							       class="form-control" id="exampleInputEmail1" placeholder="Email" readonly>
						</div>
						<label for="exampleInputEmail1">+</label>

						<div class="form-group col-xs-6">
							<button ng-click="addTcase()" type="button" class="btn btn-default btn-sm"><span
									class="glyphicon glyphicon-plus"></span></button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-1">#</div>
						<div class="col-xs-1">...</div>
						<div class="col-xs-2">N</div>
						<div class="col-xs-4">M</div>
						<div class="col-xs-4">OPERACIONES</div>
					</div>
					<div class="row" ng-repeat="tcase in testcases">
						<div class="form-group col-xs-1">
							<span class="">{{$index + 1}}</span>
						</div>
						<div class="form-group col-xs-1">
							<button ng-click="removeTcase($index)" type="button" class="btn btn-default btn-sm"><span
									class="glyphicon glyphicon-trash"></span></button>
						</div>
						<div class="form-group form-group-sm  col-xs-2">
							<input type="number" min="1" max="100" class="form-control" placeholder="1"
							       ng-model="tcase.n" id="exampleInputPassword1">
						</div>
						<div class="form-group form-group-sm  col-xs-1">
							<span>{{tcase.m}}</span>
						</div>
						<div class="form-group col-xs-1">
							<button ng-click="addOperacion($index)" type="button" class="btn btn-default btn-sm"><span
									class="glyphicon glyphicon-plus"></span></button>
						</div>

						<div class="row">

							<div class="form-group form-group-sm  col-xs-3">
								<div ng-repeat="op in tcase.operaciones">
									<input type="text" ng-model="op.valor">

									<div class="form-group col-xs-1">
										<button ng-click="removeOperacion($parent.$index, $index)" type="button"
										        class="btn btn-default btn-sm"><span
												class="">X</span></button>
									</div>
								</div>
							</div>
						</div>


					</div>
					<div class="col-xs-12">
						<button type="submit" class="btn btn-default" ng-click="save()">Submit</button>
					</div>
				</form>
			</div>

			<div class="col-md-6">
				Input:
				<pre>{{input}}</pre><br>
				Output:
				<pre>{{output}}</pre>
			</div>
		</div>
	</div>
</div>

<!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
<script src="<?= asset( 'app/lib/angular/angular.min.js' ) ?>"></script>
<script src="<?= asset( 'app/js/jquery.min.js' ) ?>"></script>
<script src="<?= asset( 'app/js/bootstrap.min.js' ) ?>"></script>
<script src="<?= asset( 'app/lib/angular/angular-resource/angular-resource.js' ) ?>"></script>

<!-- AngularJS Application Scripts -->
<script src="<?= asset( 'app/app.js' ) ?>"></script>
<script src="<?= asset( 'app/controllers/cubeController.js' ) ?>"></script>
<script src="<?= asset( 'app/models/cubeModel.js' ) ?>"></script>

</body>
</html>
