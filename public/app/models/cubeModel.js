 'use strict';
app.factory('cubeModel', [
    '$resource',
    'API_URL',
    function ($resource, API_URL) {
        var _url = API_URL + 'cube';
        var data = $resource(_url, null, {
            process: {
                method: 'GET',
                url: _url,
                isArray: true,
                params: {entrada: '@entrada'}
            },
        });
        return data;
    }

]);
