app.controller('cubeController', function($scope, $http, API_URL, cubeModel) {
    $scope.testCases = 1;
    $scope.output = '';
    $scope.testcases = [
        {n: 1, m: 1, operaciones: [{id: 1, valor: ''}],},
    ];
    $scope.addTcase = function () {
        $scope.testCases++;
        $scope.testcases.push({n: 1, m: 1, operaciones: [{id: 0, valor: ''}]});
    };
    $scope.removeTcase = function (index) {

        if ($scope.testcases.length > 1) {
            $scope.testCases--;
            $scope.testcases.splice(index, 1);
        }
    };
    $scope.addOperacion = function (padre) {
        //
        $scope.testcases[padre].m++;
        $scope.testcases[padre].operaciones.push({id: $scope.testcases[padre].m, valor: ''});

    };

    $scope.removeOperacion = function (padre,index) {
        if ($scope.testcases[padre].operaciones.length > 1) {
            $scope.testcases[padre].m--;
            $scope.testcases[padre].operaciones.splice(index, 1);
        }
    };
    $scope.setInput = function(){
        $scope.input = $scope.testCases+'\n';
        for(var i = 0; i<$scope.testcases.length; i++){
            $scope.input += $scope.testcases[i].n + ' ';
            $scope.input += $scope.testcases[i].m + '\n';
                for(var j =0; j<$scope.testcases[i].operaciones.length; j++){
                    $scope.input += $scope.testcases[i].operaciones[j].valor + '\n';
                }
        }
    }
    $scope.setOutPut = function(arr){
        $scope.output = '';
         for(var i = 0; i<arr.length; i++){
             $scope.output += arr[i]+'\n';
         }
    }

    $scope.save = function() {
        $scope.setInput();
         cubeModel.process({entrada: $scope.input}).$promise.then(function (res) {
             $scope.setOutPut(res);
         });

    }

    //delete record

});
