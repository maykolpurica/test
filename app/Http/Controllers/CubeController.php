<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Providers\cube\Process;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class CubeController extends Controller {
	public function index( Request $request ) {
		return Process::getResult( $request->entrada );
	}

	/*
	*Se inyecta un Request de modo de manejar validaciones en los inputs
	*/
	public function post_confirm( CostumRequest $request ) {
		$service_id = $request->service_id;
		$driver_id  = $request->driver_id;
		$servicio   = Service::find( $service_id );
		$driver     = Driver::find( $driver_id );
		if ( $servicio != null ) {
			/*uso 'cancelado' como ejemplo para mostrar el punto
			ademas asumo que si el resultado es alguna clase de mensaje de error el estatus en el que se
			encuentra el servicio debe ser acorde a esto*/
			if ( $servicio->status_id == Status::where( '_label', 'cancelado' )->pluck( 'id' ) ) {
				$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
			}
			/*uso 'creado' como ejemplo para mostrar el punto;
			considerando que aun no tiene conductor asignado y ademas el valor que estaba escrito era 1
			debe encontrarse en un estatus inicial pudiese ser 'creado' o 'en espera de pago'*/
			if ( $servicio->driver_id == null && $servicio->status_id == Status::where( '_label', 'creado' )->pluck( 'id' ) ) {
				if ( $servicio->user->uuid == '' ) {
					$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
				} else {
					/*Este bloque se puede colocar dentro de otro metodo
					en caso de ser usado en varios metodos de modo de no repetir codigo
					*/
					$servicio->driver_id = $driver_id;
					$servicio->status_id = Status::where( '_label', 'asignado' )->pluck( 'id' );
					$servicio->car_id    = $driver->car_id;
					$servicio->save();
					$driver->available = 0;
					$driver->save();
					//Notificar
					$pushMessage = 'Tu servicio ha sido confirmado!';
					$push        = Push::make();
					if ( $servicio->user->Type == UserType::where( '_label', 'iphone' )->pluck( 'id' ) ) { //iPhone
						$result = $push->ios( $servicio->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', array( 'serviceId' => $servicio->id ) );
					} else {
						$result = $push->android2( $servicio->user->uuid, $pushMessage, 1, 'default', 'Open', array( 'serviceId' => $servicio->id ) );
					}
				}
				$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
			} else {
				$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
			}
		} else {
			$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
		}

		return $response;
	}
}
