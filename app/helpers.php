<?php

		function textToArray($text) {
		   $text = explode("\n", $text);
		   $text = array_filter($text);
		   $text = array_values($text);
		   $text = array_map("trim", $text);
		   return $text;
		}

/**
 * @author Maykol Purica <puricamaykol@gmail.com>
 *
 * @param $array
 *
 * @return array
 *
 * Esta funcion recibe la cadena multilinea de la entrada y la convierte en un arreglo asociativo
 * Ejmplo del arreglo
 *
 *
array:2 [
	"T" => "1"
  "casos" => array:1 [
	1 => array:3 [
	"M" => "5"
      "N" => "4"
      "operaciones" => array:5 [
	1 => "UPDATE 2 2 2 4"
        2 => "QUERY 1 1 1 3 3 3"
        3 => "UPDATE 1 1 1 23"
        4 => "QUERY 2 2 2 4 4 4"
        5 => "QUERY 1 1 1 3 3 3"
      ]
    ]
  ]
]
 */
		function createTestCasesArray($array){
			$out = [];
			$nm = true;
			$current_c = 1;
			$current_op = 1;
			for($i = 0; $i < count($array); $i++){
				if($i == 0) {
					$out['T'] = $array[ $i ];
					$nm       = true;
				} elseif($i > 0){
					if($nm == true){
						list($out['casos'][$current_c]['N'],$out['casos'][$current_c]['M']) = explode(' ', $array[$i]);
						$nm = false;
					}else{
						if($current_op < $out['casos'][$current_c]['M']){
							$out['casos'][$current_c]['operaciones'][$current_op] = $array[$i];
						}
						if($current_op == $out['casos'][$current_c]['M']){
							$out['casos'][$current_c]['operaciones'][$current_op] = $array[$i];
							$current_op = 1;
							$current_c++;
							$nm = true;
						}else{
							$current_op++;
						}
					}

				}
			}
			return $out;
		}