<?php namespace App\Providers\cube;

use App\Providers\cube\PruebaContract;

class Process implements PruebaContract {

	public static function getResult($texto) {
		$cube = new cube($texto);
		return $cube->result;
	}
}