<?php

namespace App\Providers\cube;

class cube {
	public $result = [ ]; //Contiene el arreglo con los resultados de las operaciones tipo query
	private $arreglo = [ ]; //Contiene los parametros de entrada en forma de un arreglo indexado numericamente linea por linea
	public $test_cases_array = [ ]; //Contiene un arreglo que representa los parametros de entrada en forma de arreglo asociativo
	private $cubo = [ ]; //Contiene el arreglo tridimensional que representa la matriz de la prueba
	private $opParams = [ ]; //Un arreglo indexado numericamente que contiene los parametros de una operacion dada

	function __construct( $text ) {
		$this->arreglo = $this->textToArray( $text );
		$this->createTestCasesArray( $this->arreglo );
		$this->resolver();
		json_encode( $this->result );
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $text
	 *
	 * @return array
	 * Este metodo convierte la cadena multi linea de entrada en un arreglo numerico
	 */
	private function textToArray( $text ) {
		$text = explode( "\n", $text );
		$text = array_filter( $text );
		return $text;
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $array
	 *
	 * @return array
	 * Esta funcion recibe la cadena multilinea de la entrada y la convierte en un arreglo asociativo
	 * que contiene los mismos elementos ordenados
	 * Ejmplo del arreglo*/


	private function createTestCasesArray( $array ) {
		$out        = [ ];
		$nm         = true;
		$current_c  = 1;
		$current_op = 1;
		for ( $i = 0; $i < count( $array ); $i ++ ) {
			if ( $i == 0 ) {
				$out['T'] = $array[ $i ];
				$nm       = true;
			} elseif ( $i > 0 ) {
				if ( $nm == true ) {
					list( $out['casos'][ $current_c ]['N'], $out['casos'][ $current_c ]['M'] ) = explode( ' ', $array[ $i ] );
					$nm = false;
				} else {
					if ( $current_op < $out['casos'][ $current_c ]['M'] ) {
						$out['casos'][ $current_c ]['operaciones'][ $current_op ] = $array[ $i ];
					}
					if ( $current_op == $out['casos'][ $current_c ]['M'] ) {
						$out['casos'][ $current_c ]['operaciones'][ $current_op ] = $array[ $i ];
						$current_op = 1;
						$current_c ++;
						$nm = true;
					} else {
						$current_op ++;
					}
				}

			}
		}

		$this->test_cases_array = $out;
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $n
	 * Este metodo se encanrga de construir la matriz tridimensional en funcion del valor de N
	 * Recorre las tres dimensiones del arreglo acotandolas en funcion al valor de N y a cada bloque le asigna 0
	 */
	private function makeCube( $n ) {
		for ( $x = 0; $x <= $n; ++ $x ) {
			for ( $y = 0; $y <= $n; ++ $y ) {
				for ( $z = 0; $z <= $n; ++ $z ) {
					$this->cubo[ $x ][ $y ][ $z ] = 0;
				}
			}
		}
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $x1
	 * @param $y1
	 * @param $z1
	 * @param $x2
	 * @param $y2
	 * @param $z2
	 * Este metodo recibe las coordenadas de los dos bloques del arreglo y suma el valor de todos los bloques cuyas
	 * coordenadas se encuentren entre las coordenadas dadas
	 */
	private function query( $x1, $y1, $z1, $x2, $y2, $z2 ) {
		$out = 0;
		//Esto itera sobre los bloques cuya coordenada x se encuentra entre x1 y x2, su coordenada y entre y1 e y2, y su coordenada z entre z1 y z2
		for ( $x = $x1; $x <= $x2; ++ $x ) {
			for ( $y = $y1; $y <= $y2; ++ $y ) {
				for ( $z = $z1; $z <= $z2; ++ $z ) {
					$out += $this->cubo[ $x ][ $y ][ $z ];
				}
			}
		}
		reset( $this->cubo ); //regresa el puntero a la primera posicion del arreglo
		$this->result[] = $out;
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $x
	 * @param $y
	 * @param $z
	 * @param $w
	 * Metodo que se encarga de actualizar el bloque con las coordenadas dadas con el valor dado
	 */
	private function update( $x, $y, $z, $w ) {
		$this->cubo[ $x ][ $y ][ $z ] = $w;
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *         Este metodo inicializa el atributo cubo y el atributo opParams de modo de que esten en blanco en cada
	 *         iteracion
	 */
	private function cleanCube() {
		$this->cubo     = array();
		$this->opParams = array();
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $text
	 *
	 * @return bool
	 * Este metodo recibe la cadena de una operacion
	 * Retorna true si es QUERY y false si es UPDATE
	 * Dependiendo de la condicion ademas llama a los metodos que cargan los parametros de query o update en el atributo opParams
	 */
	private function isQuery( $text ) {
		$op = explode( ' ', $text );

		if ( $op[0] == 'QUERY' ) {
			$this->getQueryParams( $text );
			return true;
		} else {
			$this->getUpdateParams( $text );
			return false;
		}
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $text
	 * Este metodo toma la cadena de la operacion query y obtiene los parametros los cuales carga en el atributo
	 * opParams
	 */
	private function getQueryParams( $text ) {
		list( $op, $this->opParams[1], $this->opParams[2], $this->opParams[3], $this->opParams[4], $this->opParams[5], $this->opParams[6] ) = explode( ' ', $text );

	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *
	 * @param $text
	 * Este metodo toma la cadena de la operacion update y obtiene los parametros os cuales carga en el atributo
	 * opParams
	 */
	private function getUpdateParams( $text ) {
		list( $op, $this->opParams[1], $this->opParams[2], $this->opParams[3], $this->opParams[4] ) = explode( ' ', $text );
	}

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 *         Este metodo contiene la logica de la resolucion del cubo
	 *         No retorna nada
	 */
	private function resolver() {
		for ( $i = 1; $i <= $this->test_cases_array['T']; $i ++ ) {
			$this->makeCube( $this->test_cases_array['casos'][ $i ]['N'] );
			for ( $j = 1; $j <= $this->test_cases_array['casos'][ $i ]['M']; $j ++ ) {
				$isQuery = $this->isQuery( $this->test_cases_array['casos'][ $i ]['operaciones'][ $j ] );
				if ( $isQuery ) {
					$this->query( $this->opParams[1], $this->opParams[2], $this->opParams[3], $this->opParams[4], $this->opParams[5], $this->opParams[6] );
				} else {
					$this->update( $this->opParams[1], $this->opParams[2], $this->opParams[3], $this->opParams[4] );
				}
			}
			$this->cleanCube();
		}
	}
}