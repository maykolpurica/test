<?php

namespace App\Providers;

use App\Providers\cube\Process;
use Illuminate\Support\ServiceProvider;

class CubeServiceProvider extends ServiceProvider
{
	protected $defer = true;//esto se hace para que la clase Process no se cargue apenas se inicie la aplicación sino solo cuando se le necesite
    /**
     * Register the application services.
     *
     * @return void
     */
	public function register() {
		$this->app->bind('App\Providers\cube\PruebaContract', function(){
			return new Process();
		});
	}
	public function provides()
	{
		return ['App\Providers\cube\PruebaContract'];
	}

}
