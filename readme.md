# Prueba de ingreso Maykol Purica #

## Frameworks ##
![laravel-logo.png](https://bitbucket.org/repo/Rz7kzB/images/353751137-laravel-logo.png)
![angularjs-logo.png](http://blog.koalite.com/wp-content/uploads/AngularJS-large.png)

## Instalación ##
```sh
$ git clone https://maykolpurica@bitbucket.org/maykolpurica/test.git [directorio]
$ cd [directorio]
$ composer install
```

## Configuraciones ##
En el archivo /public/app/app.js reemplazar el directorio "test" por el directorio en el que se haya clonado
el proyecto. En caso de ser un dominio personalizado de pruebas se debe modificar la URL entera haciendo
referencia al directorio public de laravel:

```
#!js

var app = angular.module('cubeSum', ['ngResource'])
    .constant('API_URL', 'http://localhost/[directorio]/public/');
```


```
#!js

var app = angular.module('cubeSum', ['ngResource'])
    .constant('API_URL', 'dominio.personalizado/public/'
);
```
## Ejecutar ##

Para ejecutarlo simplemente se escribe la ruta hasta el directorio public de Laravel por ejemplo

[http://localhost/test/public/](Link URL)

Se agregan casos de prueba y operaciones. Se selecciona N usadon el spinner numérico. (Por falta de tiempo no pude hacer las validaciones por lo que el sistema espera que las operaciones tengan el formato adecuado es decir que las palabras QUERY y UPDATE esten bien escritas y que cada elemento este separado por un único espacio).

## Capas de la aplicación ##
* Controlador: La clase CubeController (app/Http/Controllers/CubeController.php) cumple el roll de controlador recibiendo la petición, creando el objeto necesario y retornando la respuesta a la vista. 
* Vista: La vista que se usa es app/Http/Controllers/CubeController.php y básicamente construye un html básico con sus dependencias.
* Capa de negocio: Hice uso de un ServiceProvider el cual usa una clase app/Providers/cube/cube.php que contiene toda la lógica del negocio; ver app/Providers/CubeServiceProvider.php, app/Providers/cube/PruebaContract.php y app/Providers/cube/Process.php. CubeServiceProvider básicamente se encarga de cargar la clase process permitiendo que se cargue cuando se crea un objeto o se usa el Facade. PruebaContract es  una interfaz (Laravel exige esto al momento de crear un Provider) y Process que es la clase usada por el controlador. Esta hace uso de la clase cube. Se hizo esto así con el objetivo de mantener el controlador sin lógica de negocio y dado que no existen modelos (donde algunos programadores suelen colocar la logica) El service provider era una buena opción.

## Code Refactoring ##
### 1 Malas prácticas: ###

* Dejar codigo que se usaba para debug o que se estaba probando comentando
* Multiples llamados a los mismos métodos pasandole los mismo parámetros en lugar de almacenar el resultado en una variable
* Instanciar más de una vez un mismo objeto para hacer diferentes operaciones con él en lugar de instanciarlo una sola vez y usar todos los métodos de acuerdo a lo necesitado
* Se hacen validaciones en las que se usan valores 'hard-coded' servicio->status_id user->type, estos valores no deberían estar escritos en el código puesto que estos valores pudiesen cambiar, al migrar la base de datos por ejemplo, por el contrario debería haber una función que consulte esos valores, filtrando por medio de un valor que no variará como por ejemplo un label o nombre que identifica el estatus del servicio ejm: cancelado
* Hacen falta comentarios que expliquen la lógica en el código
* Multiples return a lo largo del metodo

### 2 Como mis cambios resolvieron las malas prácticas: ###
*Se eliminaron los multiples llamados a las mismas funciones cargando la data una sola vez en una variable
*Los objetos servicio y driver se instancia una sola vez guardandose en una variable para usar los metodos necesario
*Se cambio el uso del Face Input por un objeto de la clase Request o un hijo de esta de modo de manejar validaciones de inputs en otra clase
*se cambiaron los codigos o id que se encontraban 'hardcoded' por funciones que a modo de ejemplo retornan estos ids y los retornan de modo que si cambian no exista la necesidad de editar el método 
*Todos las repsuestas se guardan en una variable la cual es retornada al final del codigo
```
#!php
/*
	*Se inyecta un Request de modo de manejar validaciones en los inputs
	*/
	public function post_confirm( CostumRequest $request ) {
		$service_id = $request->service_id;
		$driver_id  = $request->driver_id;
		$servicio   = Service::find( $service_id );
		$driver     = Driver::find( $driver_id );
		if ( $servicio != null ) {
			/*uso 'cancelado' como ejemplo para mostrar el punto
			ademas asumo que si el resultado es alguna clase de mensaje de error el estatus en el que se
			encuentra el servicio debe ser acorde a esto*/
			if ( $servicio->status_id == Status::where( '_label', 'cancelado' )->pluck( 'id' ) ) {
				$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
			}
			/*uso 'creado' como ejemplo para mostrar el punto;
			considerando que aun no tiene conductor asignado y ademas el valor que estaba escrito era 1
			debe encontrarse en un estatus inicial pudiese ser 'creado' o 'en espera de pago'*/
			if ( $servicio->driver_id == null && $servicio->status_id == Status::where( '_label', 'creado' )->pluck( 'id' ) ) {
				if ( $servicio->user->uuid == '' ) {
					$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
				} else {
					/*Este bloque se puede colocar dentro de otro metodo
					en caso de ser usado en varios metodos de modo de no repetir codigo
					*/
					$servicio->driver_id = $driver_id;
					$servicio->status_id = Status::where( '_label', 'asignado' )->pluck( 'id' );
					$servicio->car_id    = $driver->car_id;
					$servicio->save();
					$driver->available = 0;
					$driver->save();
					//Notificar
					$pushMessage = 'Tu servicio ha sido confirmado!';
					$push        = Push::make();
					if ( $servicio->user->Type == UserType::where( '_label', 'iphone' )->pluck( 'id' ) ) { //iPhone
						$result = $push->ios( $servicio->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', array( 'serviceId' => $servicio->id ) );
					} else {
						$result = $push->android2( $servicio->user->uuid, $pushMessage, 1, 'default', 'Open', array( 'serviceId' => $servicio->id ) );
					}
				}
				$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
			} else {
				$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
			}
		} else {
			$response = Response::json( array( 'error' => Error::where( '_label', 'error_deseado' )->pluck( 'id' ) ) );
		}

		return $response;
	}
```

## Preguntas ##
### ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito? ###
Esto consiste básicamente en que una clase debe tener una única responsabilidad o manejar una única característica de una aplicación o sistema.
Por ejemplo en un sistema que maneje clientes debería haber una clase que maneje todo lo relacionado con esta entidad por ejemplo, qué hacer cuando un cliente tiene más de 15 días sin pagar una factura. Otra clase debería manejar todo lo concerniente a facturas, creación, cambio de estatus etc. Esto permite que al estar las funcionalidades 'separadas' en varias clases, estas se encuentren desacopladas, esto permite (su propósito es) crear aplicaciones con bajo acoplamiento.
### ¿Qué características tiene según tú un buen código o código limpio? ###
  1. Sencillo: debe ser fácil de entender
1.   Nombres de variables y clases elocuentes: Lo suficiente para saber a plena vista de que se tratan o que contienen o en su defecto comentarios que indiquen esto al declararlos
1.   Comentado: Deben haber comentarios que expliquen el código
1.   No tener código repetido: se deben crear tantos métodos sean necesarios para no repetir el código una y otra vez
1.   No debe tener comentarios relativos al proceso de debug
1. Bien identado. Esto lo hace mas fácil de leer y entender.